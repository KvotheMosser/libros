<?xml version='1.0' encoding='utf-8'?>
<package xmlns="http://www.idpf.org/2007/opf" unique-identifier="uuid_id" version="2.0">
    <metadata xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:opf="http://www.idpf.org/2007/opf">
        <dc:identifier opf:scheme="calibre" id="calibre_id">213</dc:identifier>
        <dc:identifier opf:scheme="uuid" id="uuid_id">e592515e-9d7e-4167-aee4-430df8d7a53b</dc:identifier>
        <dc:title>Mastering Unit Testing Using Mockito and JUnit</dc:title>
        <dc:creator opf:file-as="Sujoy Acharya" opf:role="aut">Sujoy Acharya</dc:creator>
        <dc:contributor opf:file-as="calibre" opf:role="bkp">calibre (3.29.0) [https://calibre-ebook.com]</dc:contributor>
        <dc:date>2014-07-15T05:00:00+00:00</dc:date>
        <dc:description>&lt;span&gt;&lt;p&gt;&lt;b&gt;An advanced guide to mastering unit testing using Mockito and JUnit&lt;/b&gt;&lt;/p&gt;&lt;p&gt;&lt;b&gt;In Detail&lt;/b&gt;&lt;/p&gt;&lt;p&gt;It is insanity to keep doing things the same way and expect them to improve. Any program is useful only when it is functional; hence, before applying complex tools, patterns, or APIs to your production code, checking software functionality is must. Automated JUnit tests help you verify your assumptions continuously, detect side effects quickly, and also help you save time.&lt;/p&gt;&lt;p&gt;This book will provide the skills you need to successfully build and maintain meaningful JUnit test cases. You will begin with how to use advanced JUnit 4 features, improve code coverage, automate JUnit tests, monitor code quality, write JUnit tests for the database and web tier refactor legacy code, mock external dependencies using Mockito, and write testable code using test-driven development. By sequentially working through the steps in each chapter, you will quickly master the advanced JUnit features.&lt;/p&gt;&lt;p&gt;&lt;b&gt;What You Will Learn&lt;/b&gt;&lt;/p&gt;&lt;ul&gt;&lt;li&gt;Write advanced JUnit test cases using advanced JUnit 4 features&lt;/li&gt;&lt;li&gt;Automate JUnit test cases using Gradle, Maven, and Ant scripting&lt;/li&gt;&lt;li&gt;Practice continuous integration with Jenkins and Ant, Maven, and Gradle scripting&lt;/li&gt;&lt;li&gt;Monitor code quality with the SonarQube dashboard&lt;/li&gt;&lt;li&gt;Analyze static code using PMD, Checkstyle, and FindBugs&lt;/li&gt;&lt;li&gt;Master code coverage with Clover, Cobertura, EclEmma, and JaCoCo&lt;/li&gt;&lt;li&gt;Design for testability and refactor legacy code&lt;/li&gt;&lt;li&gt;Practice test-driven development with Mockito&lt;/li&gt;&lt;/ul&gt;&lt;p&gt;Downloading the example code for this book. You can download the example code files for all Packt books you have purchased from your account at http://www.PacktPub.com. If you purchased this book elsewhere, you can visit http://www.PacktPub.com/support and register to have the files e-mailed directly to you.&lt;/p&gt;&lt;/span&gt;</dc:description>
        <dc:publisher>Packt Publishing</dc:publisher>
        <dc:language>eng</dc:language>
        <dc:subject>Core Programming</dc:subject>
        <dc:subject>Information Technology/Operations</dc:subject>
        <meta content="{&quot;Sujoy Acharya&quot;: &quot;&quot;}" name="calibre:author_link_map"/>
        <meta content="2018-08-23T23:49:11.268977+00:00" name="calibre:timestamp"/>
        <meta content="Mastering Unit Testing Using Mockito and JUnit" name="calibre:title_sort"/>
    </metadata>
    <guide>
        <reference href="cover.jpg" title="Portada" type="cover"/>
    </guide>
</package>
