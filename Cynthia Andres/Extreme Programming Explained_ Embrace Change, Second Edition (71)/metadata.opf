<?xml version='1.0' encoding='utf-8'?>
<package xmlns="http://www.idpf.org/2007/opf" unique-identifier="uuid_id" version="2.0">
    <metadata xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:opf="http://www.idpf.org/2007/opf">
        <dc:identifier opf:scheme="calibre" id="calibre_id">71</dc:identifier>
        <dc:identifier opf:scheme="uuid" id="uuid_id">f5f38f90-ebd7-4971-8512-c04792b9bf3b</dc:identifier>
        <dc:title>Extreme Programming Explained: Embrace Change, Second Edition</dc:title>
        <dc:creator opf:file-as="Cynthia Andres" opf:role="aut">Cynthia Andres</dc:creator>
        <dc:creator opf:file-as="Cynthia Andres" opf:role="aut">Kent Beck</dc:creator>
        <dc:contributor opf:file-as="calibre" opf:role="bkp">calibre (3.29.0) [https://calibre-ebook.com]</dc:contributor>
        <dc:date>2004-11-16T06:00:00+00:00</dc:date>
        <dc:description>&lt;span&gt;&lt;p&gt;
“In this second edition of &lt;em&gt;Extreme Programming
Explained,&lt;/em&gt; Kent Beck organizes and presents five years’
worth of experiences, growth, and change revolving around XP. If
you are seriously interested in understanding how you and your team
can start down the path of improvement with XP, you must read this
book.”&lt;br/&gt;
&lt;br/&gt;
—&lt;b&gt;Francesco Cirillo,&lt;/b&gt; Chief Executive Officer, XPLabs
S.R.L.
&lt;/p&gt;&lt;p&gt;
“The first edition of this book told us what XP
was—it changed the way many of us think about software
development. This second edition takes it farther and gives us a
lot more of the ‘why’ of XP, the motivations and the
principles behind the practices. This is great stuff. Armed with
the ‘what’ and the ‘why,’ we can now all
set out to confidently work on the ‘how’: how to run
our projects better, and how to get agile techniques adopted in our
organizations.”&lt;br/&gt;
&lt;br/&gt;
—&lt;b&gt;Dave Thomas,&lt;/b&gt; The Pragmatic Programmers LLC
&lt;/p&gt;&lt;p&gt;
“This book is dynamite! It was revolutionary when it first
appeared a few years ago, and this new edition is equally profound.
For those who insist on cookbook checklists, there’s an
excellent chapter on ‘primary practices,’ but I urge
you to begin by truly contemplating the meaning of the opening
sentence in the first chapter of Kent Beck’s book: ‘XP
is about social change.’ You should do whatever it takes to
ensure that every IT professional and every IT manager—all
the way up to the CIO—has a copy of &lt;em&gt;Extreme Programming
Explained&lt;/em&gt; on his or her desk.”&lt;br/&gt;
&lt;br/&gt;
—&lt;b&gt;Ed Yourdon,&lt;/b&gt; author and consultant
&lt;/p&gt;&lt;p&gt;
“XP is a powerful set of concepts for simplifying the
process of software design, development, and testing. It is about
minimalism and incrementalism, which are especially useful
principles when tackling complex problems that require a balance of
creativity and discipline.”&lt;br/&gt;
&lt;br/&gt;
—&lt;b&gt;Michael A. Cusumano,&lt;/b&gt; Professor, MIT Sloan School of
Management, and author of &lt;em&gt;The Business of Software&lt;/em&gt;
&lt;/p&gt;&lt;p&gt;
“&lt;em&gt;Extreme Programming Explained&lt;/em&gt; is the work of a
talented and passionate craftsman. Kent Beck has brought together a
compelling collection of ideas about programming and management
that deserves your full attention. My only beef is that our
profession has gotten to a point where such common-sense ideas are
labeled ‘extreme.’...”&lt;br/&gt;
&lt;br/&gt;
—&lt;b&gt;Lou Mazzucchelli,&lt;/b&gt; Fellow, Cutter Business Technology
Council
&lt;/p&gt;&lt;p&gt;
“If your organization is ready for a change in the way it
develops software, there’s the slow incremental approach,
fixing things one by one, or the fast track, jumping feet first
into Extreme Programming. Do not be frightened by the name, it is
not that extreme at all. It is mostly good old recipes and common
sense, nicely integrated together, getting rid of all the fat that
has accumulated over the years.”&lt;br/&gt;
&lt;br/&gt;
—&lt;b&gt;Philippe Kruchten,&lt;/b&gt; UBC, Vancouver, British
Columbia
&lt;/p&gt;&lt;p&gt;
“Sometimes revolutionaries get left behind as the movement
they started takes on a life of its own. In this book, Kent Beck
shows that he remains ahead of the curve, leading XP to its next
level. Incorporating five years of feedback, this book takes a
fresh look at what it takes to develop better software in less time
and for less money. There are no silver bullets here, just a set of
practical principles that, when used wisely, can lead to dramatic
improvements in software development productivity.”&lt;br/&gt;
&lt;br/&gt;
—&lt;b&gt;Mary Poppendieck,&lt;/b&gt; author of &lt;em&gt;Lean Software
Development: An Agile Toolkit&lt;/em&gt;
&lt;/p&gt;&lt;p&gt;
“Kent Beck has revised his classic book based on five more
years of applying and teaching XP. He shows how the path to XP is
both easy and hard: It can be started with fewer practices, and yet
it challenges teams to go farther than ever.”&lt;br/&gt;
&lt;br/&gt;
—&lt;b&gt;William Wake,&lt;/b&gt; independent consultant
&lt;/p&gt;&lt;p&gt;
“With new insights, wisdom from experience, and clearer
explanations of the art of Extreme Programming, this edition of
Beck’s classic will help many realize the dream of
outstanding software development.”&lt;br/&gt;
&lt;br/&gt;
—&lt;b&gt;Joshua Kerievsky,&lt;/b&gt; author of &lt;em&gt;Refactoring to
Patterns&lt;/em&gt; and Founder, Industrial Logic, Inc.
&lt;/p&gt;&lt;p&gt;
“XP has changed the way our industry thinks about software
development. Its brilliant simplicity, focused execution, and
insistence on fact-based planning over speculation have set a new
standard for software delivery.”&lt;br/&gt;
&lt;br/&gt;
—&lt;b&gt;David Trowbridge,&lt;/b&gt; Architect, Microsoft
Corporation
&lt;/p&gt;&lt;p&gt;Accountability. Transparency. Responsibility. These are not
words that are often applied to software development.&lt;/p&gt;&lt;p&gt;In this completely revised introduction to Extreme Programming
(XP), Kent Beck describes how to improve your software development
by integrating these highly desirable concepts into your daily
development process.&lt;/p&gt;&lt;p&gt;The first edition of &lt;em&gt;Extreme Programming Explained&lt;/em&gt; is a
classic. It won awards for its then-radical ideas for improving
small-team development, such as having developers write automated
tests for their own code and having the whole team plan weekly.
Much has changed in five years. This completely rewritten second
edition expands the scope of XP to teams of any size by suggesting
a program of continuous improvement based on:&lt;/p&gt;&lt;li&gt;&lt;p&gt;Five core values consistent with excellence in software
development&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;Eleven principles for putting those values into action&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;Thirteen primary and eleven corollary practices to help you
push development past its current business and technical
limitations&lt;/p&gt;&lt;/li&gt;&lt;p&gt;Whether you have a small team that is already closely aligned
with your customers or a large team in a gigantic or multinational
organization, you will find in these pages a wealth of ideas to
challenge, inspire, and encourage you and your team members to
substantially improve your software development.&lt;/p&gt;&lt;p&gt;You will discover how to:&lt;/p&gt;&lt;li&gt;&lt;p&gt;Involve the whole team—XP style&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;Increase technical collaboration through pair programming and
continuous integration&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;Reduce defects through developer testing&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;Align business and technical decisions through weekly and
quarterly planning&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;Improve teamwork by setting up an informative, shared
workspace&lt;/p&gt;&lt;/li&gt;&lt;p&gt;You will also find many other concrete ideas for improvement,
all based on a philosophy that emphasizes simultaneously increasing
the humanity and effectiveness of software development.&lt;/p&gt;&lt;p&gt;Every team can improve. Every team can begin improving today.
Improvement is possible—beyond what we can currently imagine.
&lt;b&gt;&lt;em&gt;Extreme Programming Explained, Second Edition,&lt;/em&gt;&lt;/b&gt; offers
ideas to fuel your improvement for years to come.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;br/&gt;&lt;/p&gt;&lt;/span&gt;</dc:description>
        <dc:publisher>Addison-Wesley Professional</dc:publisher>
        <dc:language>eng</dc:language>
        <dc:subject>Agile</dc:subject>
        <dc:subject>Core Programming</dc:subject>
        <meta content="{&quot;Kent Beck&quot;: &quot;&quot;, &quot;Cynthia Andres&quot;: &quot;&quot;}" name="calibre:author_link_map"/>
        <meta content="2018-08-23T23:27:38.447426+00:00" name="calibre:timestamp"/>
        <meta content="Extreme Programming Explained: Embrace Change, Second Edition" name="calibre:title_sort"/>
    </metadata>
    <guide>
        <reference href="cover.jpg" title="Portada" type="cover"/>
    </guide>
</package>
