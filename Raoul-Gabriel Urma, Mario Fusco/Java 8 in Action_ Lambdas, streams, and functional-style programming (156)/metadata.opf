<?xml version='1.0' encoding='utf-8'?>
<package xmlns="http://www.idpf.org/2007/opf" unique-identifier="uuid_id" version="2.0">
    <metadata xmlns:dc="http://purl.org/dc/elements/1.1/" xmlns:opf="http://www.idpf.org/2007/opf">
        <dc:identifier opf:scheme="calibre" id="calibre_id">156</dc:identifier>
        <dc:identifier opf:scheme="uuid" id="uuid_id">808e8f05-9d93-4cfa-8290-fb5ccdf0d4d4</dc:identifier>
        <dc:title>Java 8 in Action: Lambdas, streams, and functional-style programming</dc:title>
        <dc:creator opf:file-as="Raoul-Gabriel Urma, Mario Fusco, and Alan Mycroft" opf:role="aut">Raoul-Gabriel Urma, Mario Fusco</dc:creator>
        <dc:creator opf:file-as="Raoul-Gabriel Urma, Mario Fusco, and Alan Mycroft" opf:role="aut">Alan Mycroft</dc:creator>
        <dc:contributor opf:file-as="calibre" opf:role="bkp">calibre (3.29.0) [https://calibre-ebook.com]</dc:contributor>
        <dc:date>2014-08-15T05:00:00+00:00</dc:date>
        <dc:description>&lt;span&gt;&lt;p&gt;&lt;em&gt;Java 8 in Action&lt;/em&gt; is a clearly written guide to to the new
features of Java 8. The book covers lambdas, streams, and
functional-style programming. With Java 8’s functional
features you can now write more concise code in less time, and also
automatically benefit from multicore architectures. It’s time
to dig in!







&lt;/p&gt;&lt;p&gt;&lt;b&gt;Summary&lt;/b&gt;&lt;/p&gt;&lt;p&gt;&lt;b&gt;About the Book&lt;/b&gt;&lt;/p&gt;&lt;p&gt;Every new version of Java is important, but Java 8 is a game
changer. Java 8 in Action is a clearly written guide to the new
features of Java 8. It begins with a practical introduction to
lambdas, using real-world Java code. Next, it covers the new
Streams API and shows how you can use it to make collection-based
code radically easier to understand and maintain. It also explains
other major Java 8 features including default methods, Optional,
CompletableFuture, and the new Date and Time API.&lt;/p&gt;&lt;p&gt;This book is written for programmers familiar with Java and
basic OO programming.&lt;/p&gt;&lt;p&gt;&lt;b&gt;What’s Inside&lt;/b&gt;&lt;/p&gt;&lt;li&gt;&lt;p&gt;How to use Java 8’s powerful new features&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;Writing effective multicore-ready applications&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;Refactoring, testing, and debugging&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;Adopting functional-style programming&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;Quizzes and quick-check questions&lt;/p&gt;&lt;/li&gt;&lt;p&gt;&lt;b&gt;About the Authors&lt;/b&gt;&lt;/p&gt;&lt;p&gt;&lt;b&gt;Raoul-Gabriel Urma&lt;/b&gt; is a software engineer, speaker,
trainer, and PhD candidate at the University of Cambridge. &lt;b&gt;Mario
Fusco&lt;/b&gt; is an engineer at Red Hat and creator of the lambdaj
library. &lt;b&gt;Alan Mycroft&lt;/b&gt; is a professor at Cambridge and
cofounder of the Raspberry Pi Foundation.&lt;/p&gt;&lt;/span&gt;</dc:description>
        <dc:publisher>Manning Publications</dc:publisher>
        <dc:language>eng</dc:language>
        <dc:subject>Certification</dc:subject>
        <dc:subject>Java</dc:subject>
        <meta content="{&quot;Raoul-Gabriel Urma, Mario Fusco&quot;: &quot;&quot;, &quot;Alan Mycroft&quot;: &quot;&quot;}" name="calibre:author_link_map"/>
        <meta content="2018-08-23T23:42:23.769979+00:00" name="calibre:timestamp"/>
        <meta content="Java 8 in Action: Lambdas, streams, and functional-style programming" name="calibre:title_sort"/>
    </metadata>
    <guide>
        <reference href="cover.jpg" title="Portada" type="cover"/>
    </guide>
</package>
